class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        "/todos/$username"(controller:"todo",action:"showTodosByUser")

        "/api/todos"(resources:"todo")
        "/api/tags"(resources:"tag")
        "/api/categories"(resources:"category")

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
