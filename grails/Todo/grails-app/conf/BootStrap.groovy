import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        try {

            def roleAdmin = new Role(authority: "ROLE_ADMIN").save()
            def roleUser = new Role(authority: "ROLE_BASIC").save()

            def userAdmin = new User(username: "admin", password: "admin", name: "admin", surnames: "admin istrador", confirmPassword: "", email: "admin@admin.es").save()
            def userUsu1 = new User(username: "usu1", password: "usu1", name: "usu1", surnames: "ario1", confirmPassword: "", email: "usu1@usu1.es").save()
            def userUsu2 = new User(username: "usu2", password: "usu2", name: "usu2", surnames: "ario2", confirmPassword: "", email: "usu2@usu2.es").save()

            PersonRole.create(userAdmin, roleAdmin)
            PersonRole.create(userUsu1, roleUser)
            PersonRole.create(userUsu2, roleUser)

            def categoryHome = new Category(name: "Hogar").save()
            def categoryJob = new Category(name: "Trabajo").save()

            def tagEasy = new Tag(name: "Fácil").save()
            def tagDifficult = new Tag(name: "Difícil").save()
            def tagArt = new Tag(name: "Arte").save()
            def tagRoutine = new Tag(name: "Rutina").save()
            def tagKitchen = new Tag(name: "Cocina").save()

            def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1, realizada: false, user: userUsu1)
            def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2, realizada: false, user: userUsu1)
            def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4, realizada: false, user: userUsu2)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), realizada: false, user: userAdmin)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.save()

        } catch (Exception e) {
            System.out.println("Error en BootStrap: " + e.printStackTrace());
        }
    }
    def destroy = {}

}
