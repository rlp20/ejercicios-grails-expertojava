/* Añade aquí la implementación del factorial en un closure */

def factorial =  {int num ->
    numFactorial = 1
    for (elemento in num..<1){
        numFactorial *= elemento
    }
    return numFactorial
}

def listaFact = [1,2,3,4,5,6,7,8,9]
listaFact.each { elemento->
    println factorial(elemento)
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

/* Crear dos closures llamados ayer y mañana que devuelvan las fechas correspondientes a los días anterior y posterior a la fecha pasada
Podemos crear fechas mediante la sentencia new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20")*/
def yesterday = { fecha ->
    def ayer = fecha - 1
    return new Date().parse("d/M/yyyy H:m:s",ayer.format("d/M/yyyy  H:m:s"))
}

def tomorrow = { fecha ->
    def tomorrow = fecha + 1
    return new Date().parse("d/M/yyyy H:m:s",tomorrow.format("d/M/yyyy  H:m:s"))
}

def hoy = new Date()
def ayer = hoy -1;
def manhiana = hoy +1

assert yesterday(hoy).toString() == ayer.toString()
assert tomorrow(hoy).toString() == manhiana.toString()

/*Crear una lista de fechas y utilizar los closures recién creados para obtener las fechas del día anterior y posterior a todos los elementos de la lista.*/

listaFechas = [hoy, hoy+1, hoy+2]
(listaFechas.size()-1..0).each{
    def fecha = listaFechas[it];
    println("La fecha anterior a " +fecha + " es: " + yesterday(fecha)+ " y la posterior: " + tomorrow(fecha))
}