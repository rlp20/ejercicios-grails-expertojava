package es.ua.expertojava.todo

import grails.transaction.Transactional
import org.grails.datastore.gorm.finders.FindAllByFinder

import static org.springframework.http.HttpStatus.NO_CONTENT

class CategoryService {

    def serviceMethod(){

    }

    def delete(Category categoryInstance) {
        categoryInstance.todos.each { todo -> todo.setCategory(null)
        }
        categoryInstance.delete flush: true

//    def serviceDeleteCategory(Category cat) {
//        def categories = Category.findAllByTitle(cat.name);
//        categories.each{ category->
//            category.Todo = null;
//        }
//        cat.delete();
//    }

    }

    def listCategories(){
        return  Category.findAll();
    }

}
