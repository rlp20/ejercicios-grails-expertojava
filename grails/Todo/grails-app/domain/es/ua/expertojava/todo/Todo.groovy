package es.ua.expertojava.todo

class Todo {

    String title
    String description
    Date date
    Date reminderDate
    String url
    Category category
    Boolean realizada
    //Auditoria
    Date dateCreated
    Date lastUpdated
    //
    Date dateDone

    User user

    static hasMany = [tags:Tag]
    static belongsTo = [Tag]

    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:true,
                validator: { val, obj ->
                    if (val && obj.date) {
                        return val.before(obj?.date)
                    }
                    return true
                }
        )
        url(nullable:true, url:true)
        category(nullable:true)
        realizada(blank:false)
        dateDone(nullable:true)
    }

    String toString(){
        "todo"
    }
}
