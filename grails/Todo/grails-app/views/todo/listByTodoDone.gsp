<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<div id="edit-todo" class="content scaffold-edit" role="main">
    <h1>Tareas completadas en n horas hacia atrás</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${todoInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${todoInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <a href="#edit-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
    <div class="nav" role="navigation">
        <ul>
            <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        </ul>
    </div>
    <g:form url="[resource:todoInstance, action:'listTodoByCategory']">

        <div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'numHoras', 'error')} required">
            <label for="numHoras">
                <g:message code="todo.numHoras.label" default="Numero de horas" />
                <span class="required-indicator">*</span>
            </label>
            <g:textField name="numHoras" required="" value=""/>

        </div>


        <fieldset class="buttons">
            <g:actionSubmit class="update" action="listTodoMenosHoras" value="mostrar ultimas tareas terminadas" />
        </fieldset>



    </g:form>
</div>
</body>
</html>>