package es.ua.expertojava.todo

class TodoTagLib {
    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'todo'

//    def printIconFromBoolean = { attrs, body ->
//        String outIcon="iconOK.gif";
//        if (!attrs['value']){
//            outIcon="iconNOK.gif";
//        }
//        def mkp = new groovy.xml.MarkupBuilder(out)
//        mkp.img(src:outIcon)
//    }


//    def printIconFromBoolean = { attrs ->
//        def imagen = "iconKO.png";
//        if(!attrs['value']) {
//            out <<asset.image(src:"iconKO.png")
//        } else {
//            out <<asset.image(src:"iconOK.png")
//        }
//    }


    def printIconFromBoolean = { attrs ->
        def imagen = 'iconOK.png';
        if(!attrs['value']) {
            imagen = 'iconNOK.png'
        }
        out <<asset.image(src:imagen)
    }
}
