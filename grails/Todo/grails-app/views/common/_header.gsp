<div id="menu">
    %{--<nobr>--}%
        %{--<g:if test="${isUserLoggedIn}">--}%
            %{--<b>${userInstance?.name} ${userInstance?.surnames}</b> |--}%
            %{--<g:link controller="user" action="logout">Logout</g:link>--}%
        %{--</g:if>--}%
        %{--<g:else>--}%
            %{--<g:link controller="user" action="login">Login</g:link>--}%
        %{--</g:else>--}%
    %{--</nobr>--}%
    <nobr>
    %{--meter el logout en un form e indicarle que es method=POST--}%
        <sec:ifLoggedIn>
            <b>Bienvenido <sec:username/></b>
            <form name="submitForm" method="POST" action="${createLink(controller: 'logout')}">
                <input type="hidden" name="" value="">
                %{--Función de js que simula el submit de un formulario poniendo en marcha el action del form.--}%
                <a href="javascript:document.submitForm.submit()">Logout</a>
            </form>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <g:link controller="login" action="auth">Login</g:link>
        </sec:ifNotLoggedIn>
    </nobr>
</div>

