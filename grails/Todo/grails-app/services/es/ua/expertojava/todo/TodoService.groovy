package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory

class TodoService {

    def springSecurityService

    def serviceMethod() {

    }

    def delete(todoInstance) {
        def tagsList = new ArrayList()
        todoInstance.tags.each { tag ->
            tagsList.add(tag)
        }
        tagsList.each { tag->
            todoInstance.removeFromTags(tag)
        }
        todoInstance.delete flush:true
    }

    def getTodosByCategory(listaCategorias){
        User usuarioLog = springSecurityService.getCurrentUser();
        return Todo.executeQuery("select o from Todo as o where o.user = :usuario and o.category.id in (:list)  order by o.date",[list: listaCategorias, usuario: usuarioLog])
    }

    def showUserTodosService(){
        Todo.findAllByUser (springSecurityService.getCurrentUser())
    }

    def saveTodo(Todo todoInstance){
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.realizada){
            Date now = new Date();
            todoInstance.dateDone = now;
        } else {
            todoInstance.dateDone = null;
        }
        todoInstance.user = springSecurityService.getCurrentUser()

        todoInstance.save flush:true
    }

    def lastTodosDone(Integer horas){
        def fechaActual = new Date();
        def fechaActualMenosNHoras
        use( TimeCategory ) {
            fechaActualMenosNHoras = fechaActual - horas.hours
        }
        List lista = Todo.findAllByDateDoneBetween(fechaActualMenosNHoras, fechaActual)
        return lista
    }

    def listTodosByUser(User user) {
        List lista = Todo.findAllByUser(user);
        return lista
    }
}
