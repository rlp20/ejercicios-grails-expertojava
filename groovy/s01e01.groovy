import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class Todo {
    String titulo;
    String descripcion;

    public Todo() {}

    public Todo(String tit, String des) {
        this.titulo = tit;
        this.descripcion = des;
    }

//    public String getTitulo(){
//        return titulo;
//    }

//    public void setTitulo(String tit){
//        this.titulo = tit;
//    }

//    public String getDescripcion(){
//        return descripcion;
//    }

//    public void setDescripcion(String des){
//        this.descripcion = des;
//    }

}

todos = []
todos.add(new Todo("Lavadora","Poner lavadora"));
todos.add(new Todo("Impresora","Comprar cartuchos impresora"));
todos.add(new Todo("Películas","Devolver películas videoclub"));

todos.each{println it.titulo + " " + it.descripcion}
