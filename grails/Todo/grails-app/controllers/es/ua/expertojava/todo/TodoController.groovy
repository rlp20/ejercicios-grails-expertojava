package es.ua.expertojava.todo



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {
    def springSecurityService
    CategoryService categoryService
    TodoService todoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond todoService.showUserTodosService(), model:[todoInstanceCount: Todo.count()]
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def showTodosUser(){
        respond todoService.showUserTodosService(), view:'index'
    }

    def create() {
        respond  new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }
        todoService.saveTodo(todoInstance)

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.title])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }
        todoService.saveTodo(todoInstance)
	//todoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        todoService.delete(todoInstance)


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.title])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
                model:[todoInstanceCount: todoService.countNextTodos(days)],
                view:"index"
    }

    def listByCategory() {
        respond Todo.list(params), model:[categoryInstanceList: categoryService.listCategories()]
    }

    def listTodoByCategory(){
        List<String> listaCategorias = new ArrayList<>();
        params.get("categoria").each { idCategoria ->
                listaCategorias.add(Long.valueOf(idCategoria));
        }
        respond Todo.list(params), model:[listaTodos: todoService.getTodosByCategory(listaCategorias )]
    }

    def listByTodoDone(){

    }
    def listTodoMenosHoras(){
        def horas = 0;
        horas = Integer.valueOf (params.get("numHoras"))
        respond Todo.list(params), model:[listaTodos: todoService.lastTodosDone(horas) ], view:'listTodoByCategory'
    }

    def showTodosByUser(){
        def username = params.username
        respond Todo.list(params), model:[listaTodosByUser: todoService.listTodosByUser(springSecurityService.getCurrentUser()) ], view:'listTodoByUser'
    }

}
