<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<div id="edit-todo" class="content scaffold-edit" role="main">
    <h1>Seleccionar categorias</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${todoInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${todoInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <a href="#edit-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
    <div class="nav" role="navigation">
        <ul>
            <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        </ul>
    </div>
    <g:form url="[resource:todoInstance, action:'listTodoByCategory']">
    <table>
        <thead>
        <tr>
            <g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'nombre')}" />
        </tr>
        </thead>
        <tbody>
        <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                %{--<td><g:link action="show">${fieldValue(bean: categoryInstance, field: "name")}</g:link></td>--}%

                <td><g:checkBox name="categoria" id="${fieldValue(bean: categoryInstance, field: "id")}" value="${fieldValue(bean: categoryInstance, field: "id")}"/>${fieldValue(bean: categoryInstance, field: "name")}</td>

            </tr>
        </g:each>

        </tbody>
    </table>
       <fieldset class="buttons">
            <g:actionSubmit class="update" action="listTodoByCategory" value="Tareas con la categoria seleccionada" />
            <g:actionSubmit class="update" action="listByTodoDone" value="Tareas completadas en n horas menos" />
        </fieldset>
    </g:form>
</div>
</body>
</html>>