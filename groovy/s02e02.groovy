class Calculadora {

    Integer operador1
    Integer operador2
    
    Double getResultado(op1, op2, operacion) {
        switch(operacion) {
            case(1): return op1 + op2; break
            case(2): return op1 - op2; break
            case(3): return op1 * op2; break
            case(4): return op1 / op2; break
        }
    }
    
    int muestraOpciones(){
        boolean isValid = false
        boolean isFirstTime = true
        int opcion
        def mensaje =  """\nElige una opcion:          
        1 .- Sumar
        2 .- Restar
        3 .- Multiplicar
        4 .- Dividir
        5 .- Salir\n
        Escoge tu opción: """
       
        while (!isValid){
            opcion = getOperando(mensaje)
            if(opcion !=1 && opcion !=2 && opcion !=3 && opcion !=4 && opcion !=5)
                mensaje = "Por favor escoge una opcion valida: "
            else
                isValid = true;
        }   
        return opcion 
    }
          
    def getOperando = { mensaje ->
        def isValid = false
        int numero
        while (!isValid){
            try{
                def entrada = System.console().readLine mensaje
                if((numero = Integer.parseInt(entrada.trim())) instanceof Integer ){
                    isValid = true
                }
            }catch(NumberFormatException e){
                mensaje = "Por favor introduce opcion valida: "
            }
        }
        return numero
    }
    
    int iniApp() {
        
        def operacion = muestraOpciones()
    
        operador1 = getOperando("Introduce op1: ")
        operador2 = getOperando("Introduce op2: ")
            
        Double resultado
        resultado = getResultado(operador1, operador2, operacion)
        println "El resultado es: " + resultado + "\n"
        return resultado
    }        
    
    
    static main(args){
        Calculadora calculadora  = new Calculadora();
        calculadora.iniApp()
    }  
}