package es.ua.expertojava.todo

import es.ua.expertojava.todo.Todo

class Category {

    String name
    String description

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
    }

    public Category(String nameCategory) {
        this.name = nameCategory;
    }

    String toString(){
        name
    }
}
